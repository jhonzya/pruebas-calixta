<?php

namespace Aspects;

use Go\Core\AspectKernel;
use Go\Core\AspectContainer;

/**
 * Created by PhpStorm.
 * User: Jonathan Salinas
 * Date: 11/07/2016
 * Time: 15:13
 */
class AspectsRegister extends AspectKernel{


    /**
     * Configure an AspectContainer with advisors, aspects and pointcuts
     *
     * @param AspectContainer $container
     *
     * @return void
     */
    protected function configureAop(AspectContainer $container){
        $container->registerAspect(new DebugProfilerGral());
    }
}