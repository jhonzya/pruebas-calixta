<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Salinas
 * Date: 11/07/2016
 * Time: 16:48
 */

namespace Aspects;

use Go\Aop\Aspect;
use Go\Aop\Intercept\FieldAccess;
use Go\Aop\Intercept\MethodInvocation;
use Go\Lang\Annotation\After;
use Go\Lang\Annotation\Before;
use Go\Lang\Annotation\Around;
use Go\Lang\Annotation\Pointcut;
use Go\Lang\Annotation\DeclareParents;

use Monolog;

class DebugProfilerGral implements Aspect{

    protected $profiler;
    protected $debug;

    public function __construct(){
        $this->profiler = new Monolog\Logger('PROFILER');
        $this->profiler->pushHandler(new Monolog\Handler\StreamHandler( __DIR__.'/../storage/logs/profiler.log'), Monolog\Logger::INFO);

        $this->debug = new Monolog\Logger('DEBUG');
        $this->debug->pushHandler(new Monolog\Handler\StreamHandler( __DIR__.'/../storage/logs/debug.log'), Monolog\Logger::INFO);
    }

    /**
     * @param MethodInvocation $invocation
     * @Around("execution(public App\Pruebas->*(*)) || @execution(Aspects\DebugProfileAnnotation)")
     */

    public function aroundMethodExecution(MethodInvocation $invocation){
        $time = microtime(true);
        $arreglo = ['Tiempo' => $time];
        config('profiler.HABILITADO') ? $this->profiler($invocation, $arreglo, 'Inicio') : null;

        $invocation->proceed();

        $arreglo = ['Tiempo' => microtime($time), 'Duración' => microtime(true) - $time];
        config('profiler.HABILITADO') ? $this->profiler($invocation, $arreglo, 'Final') : null;
    }

    /**
     * @param $invocation MethodInvocation
     * @param $times Arreglo de los tiempos
     * @param $bandera  Inicio/Final
     */
    private function profiler($invocation, $times, $bandera){
        if( config('profiler.GENERAL') || (\Cache::get(csrf_token().'_profiler',null) !== null) ){
            $objeto = $invocation->getThis();
            $nombre = (is_object($objeto) ? get_class($objeto) : $objeto).($invocation->getMethod()->isStatic() ? '::' : '->').$invocation->getMethod()->getName().'()';

            $this->profiler->info( $bandera, [
                'Session' => csrf_token(),
                'Método' => $nombre,
                $times
           ] );

            if(config('profiler.DEBUG')){
                $this->debug->info( $bandera, [
                    'Session' => csrf_token(),
                    'Método' => $nombre,
                    'Argumentos' => $invocation->getArguments(),
                    'Propiedades' => get_object_vars($objeto)
                ]);
            }
        }
    }

}