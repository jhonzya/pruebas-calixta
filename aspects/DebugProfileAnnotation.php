<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Salinas
 * Date: 13/07/2016
 * Time: 10:12
 */

namespace Aspects;
use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */

class DebugProfileAnnotation extends Annotation{
}