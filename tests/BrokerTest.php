<?php

/*use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;*/

use App\lib\Aspects\Broker;

class BrokerTest extends \PHPUnit_Framework_TestCase
{

    public function testBrokerCanBuyShares(){
        $broker = new Broker('Jhon', 1);
        echo "Ejecutando";
        $this->assertEquals(500, $broker->buy('GOOGL', 100, 5));
    }

    public function testBrokerCanSellShares(){
        $broker = new Broker('Jhon', 1);
        echo "Ejecutando";
        $this->assertEquals(500, $broker->sell('YAHOO', 50, 10));
    }
}
