<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Salinas
 * Date: 15/07/2016
 * Time: 11:53
 */

namespace App\Providers;

use App\Usuario;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Cache;
use DB;

class CacheUserProvider implements UserProvider{

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier){
        return Cache::remember('usuario_id_'.$identifier, 60, function () use ($identifier){
            return Usuario::find($identifier);
        });
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token){
        // TODO: Implement retrieveByToken() method.
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token){
        // TODO: Implement updateRememberToken() method.
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials){
        return Usuario::join('clientes', 'clientes.id_cliente', '=', 'usuario.id_cliente')->
            select(DB::raw("password('".$credentials['password']."') as password_mysql, password, id_usuario"))
            ->where([
                ['usuario.email', $credentials['email']],
                ['usuario.id_portal', config('portal.PORTAL_ID')],
                ['usuario.estado', 100],
                ['clientes.estado', 100],
                ['clientes.id_portal', config('portal.PORTAL_ID')]
            ])->first();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials){
        if( $user->getAuthPassword() === $user->password_mysql)
            return true;
        return false;
    }
}