<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Salinas
 * Date: 15/07/2016
 * Time: 12:06
 */

namespace App\Providers;

use App\Usuario;
use Auth;
use Illuminate\Support\ServiceProvider;

class CacheEloquentServiceProvider extends ServiceProvider{

    public function boot(){
        Auth::provider('cache_eloquent', function (){
            return new CacheUserProvider();
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}