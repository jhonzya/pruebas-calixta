<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    \App\User::all();
    return view('welcome');
});

Route::get('prueba', function(){
    \App\User::all();
    \App\User::all();
    \App\User::all();
    \App\User::all();

    return redirect('/');
});

Route::get('redis', function(){
    return Redis::get('foo');
});

Route::get('cache', function (){
    return Cache::get('ASPECTS', 'No');
});

Route::get('broker', function (){
    $pruebas = new \App\lib\Aspects\Broker(0);
    $pruebas->suma();
});