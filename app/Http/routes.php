<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
    Log::info('QUERY', [
        'SQL' => $query->sql,
        'BINDINGS' => $query->bindings,
        'TIEMPO' => $query->time,
    ]);
});


Route::get('/', function (){
    return view('welcome');
})->middleware('auth');

Route::get('login', function (){
    return view('auth.login');
})->middleware('guest');

Route::get('logout', 'AuthController@logout');
Route::post('login', 'AuthController@authenticate');


Route::get('logs', 'SumasController@login')->middleware('web');
/*
Route::get('/', function(){
    return view('home');}
);

Route::get('sumas', "SumasController@sumar")->middleware('auth');

Route::get('broker', "SumasController@broker")->middleware('web');
Route::get('constantes', "SumasController@constantes")->middleware('web');
Route::get('logs', 'SumasController@login')->middleware('web');

Route::get('login', function(){
    echo "hola";
});
*/