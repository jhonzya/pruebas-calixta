<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class LogsInfoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        Log::info('requests.start', [
            'uri' => $request->path(),
            'time' => LARAVEL_START,
            'difference' => microtime(true) - LARAVEL_START
        ]);
        return $next($request);
    }

    public function terminate($request, $response){
        Log::info('requests.end', [
            'uri' => $request->path(),
            'time' => microtime(true), 'difference' => microtime(true) - LARAVEL_START
        ]);
    }
}
