<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;

use Auth;
use DB;
use Validator;
use App\Http\Requests;

class AuthController extends Controller{

    public function authenticate(Request $request){
        $validacion = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validacion->fails())
            return back()->withInput()->withErrors($validacion->errors());

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            return redirect()->intended('/');

        return back()->withInput()->withErrors(['auth' => 'Datos incorrectos']);
    }

    public function logout(){
        Auth::logout();
        return redirect('login');
    }

}
