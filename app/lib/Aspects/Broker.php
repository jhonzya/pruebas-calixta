<?php

namespace App\lib\Aspects;
use Aspects\DebugProfileAnnotation;

/**
 * Created by PhpStorm.
 * User: Administrador
 * Date: 06/07/2016
 * Time: 19:20
 */
class Broker{

    private $contador;

    /**
     * @DebugProfileAnnotation
     */

    public function __construct($inicio){
        $this->contador = $inicio;
    }

    private function sumador($fin){
        $comentariosDebug = ['Hola'];
        for ($i = $this->contador; $i < $fin; $i++){
            echo $i;
        }
    }

    public function suma(){
        for ($i = 0; $i < 5; $i++)
            $this->sumador($this->contador + 3);
    }
}