<?php

namespace App\lib\Aspects;

use Go\Core\AspectKernel;
use Go\Core\AspectContainer;

//use Monolog;

/**
 * Created by PhpStorm.
 * User: Administrador
 * Date: 06/07/2016
 * Time: 19:12
 */
class ApplicationAspectKernel extends AspectKernel{

    //protected $logger;

    /*public function __construct(){
        $this->logger = new Monolog\Logger('test');
        $this->logger->pushHandler(new Monolog\Handler\StreamHandler( __DIR__.'/../../../storage/logs/register.log'));
    }*/

    /**
     * Configure an AspectContainer with advisors, aspects and pointcuts
     *
     * @param AspectContainer $container
     *
     * @return void
     */
    protected function configureAop(AspectContainer $container){
        //echo \Illuminate\Cache\Repository::get('ASPECTS', 'No');
        
        
        $container->registerAspect(new BrokerAspect());
    }

}