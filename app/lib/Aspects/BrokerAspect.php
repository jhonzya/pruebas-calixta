<?php

namespace App\lib\Aspects;

use Go\Aop\Aspect;
use Go\Aop\Intercept\FieldAccess;
use Go\Aop\Intercept\MethodInvocation;
use Go\Lang\Annotation\After;
use Go\Lang\Annotation\Before;
use Go\Lang\Annotation\Around;
use Go\Lang\Annotation\Pointcut;
use Go\Lang\Annotation\DeclareParents;


/**
 * Created by PhpStorm.
 * User: Administrador
 * Date: 06/07/2016
 * Time: 19:16
 */
class BrokerAspect implements Aspect{

    /**
     * @param MethodInvocation $invocation Invocation
     * @Before("execution(public App\lib\Aspects\Broker->sumador(*))")
     */
    public function beforeMethodExecution(MethodInvocation $invocation){
        echo $invocation->getMethod()->getName();
        /*for ($i = 0; $i < 100000; $i++){
            echo $i;
        }*/
    }

}