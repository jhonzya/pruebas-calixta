<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Salinas
 * Date: 13/07/2016
 * Time: 14:03
 */

namespace App\Traits;
use Monolog;

trait ComentarioDebug{

    public function comentar($texto){
        if( config('profiler.HABILITADO') && config('profiler.DEBUG') ){
            $debug = new Monolog\Logger('DEBUG');
            $debug->pushHandler(new Monolog\Handler\StreamHandler( __DIR__.'../../../storage/logs/debug.log'), Monolog\Logger::INFO);

            $debug->info('Comentarios', [
                'Contenido' => $texto
            ]);
        }
    }

}