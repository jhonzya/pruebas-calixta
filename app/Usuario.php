<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class Usuario extends Model implements Authenticatable{

    protected $table = 'usuario';
    protected $hidden = ['password'];
    protected $primaryKey = "id_usuario";

    public function cliente(){
        return $this->belongsTo('App\Cliente', 'id_cliente', 'id_cliente');
    }
    
    // Contract Authenticable
    public function getAuthIdentifierName(){
        return 'id_usuario';
    }

    public function getAuthIdentifier(){
        return $this->id_usuario;
    }

    public function getAuthPassword(){
        return $this->password;
    }

    public function getRememberToken(){
        return null;
    }

    public function setRememberToken($value){
    }

    public function getRememberTokenName(){
        return null;
    }

    /**
     * Overrides the method to ignore the remember token.
     */
    public function setAttribute($key, $value){
        if(!($key == $this->getRememberTokenName()))
            parent::setAttribute($key, $value);
    }
}
